/*
This file is part of SystemTestPortal.
Copyright (C) 2018  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package monitoring

import (
	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestDurationSeconds = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Namespace: "stp",
			Subsystem: "http",
			Name:      "request_duration_seconds",

			Help: "Observes the response times for HTTP handlers",
		},
		[]string{
			"code",
			"method",
		},
	)

	goCollector = prometheus.NewGoCollector()
)

// RequestDurationSeconds returns a metric that can be used to observe request durations for HTTP handlers.
func RequestDurationSeconds() prometheus.ObserverVec {
	return requestDurationSeconds
}

// Gatherer returns a prometheus.Gatherer that can be used to gather metrics from the application.
func Gatherer() prometheus.Gatherer {
	r := prometheus.NewRegistry()

	r.MustRegister(requestDurationSeconds, goCollector)

	return r
}
