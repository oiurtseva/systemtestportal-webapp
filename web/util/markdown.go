/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package util

import (
	"bytes"
	"path"
	"regexp"
	"strings"

	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/stp-team/systemtestportal-webapp/config"
	"gopkg.in/russross/blackfriday.v2"
)

var extensions = blackfriday.CommonExtensions
var renderer = blackfriday.NewHTMLRenderer(blackfriday.HTMLRendererParameters{})
var sanitizer = bluemonday.UGCPolicy().AllowAttrs("class").Matching(regexp.MustCompile(`^markdown-paragraph$`)).OnElements("p")

func ParseMarkdown(markdown string) string {
	markdown = addDataDirToAttachments(markdown)
	md := blackfriday.New(blackfriday.WithExtensions(extensions))
	ast := md.Parse([]byte(markdown))
	var buff bytes.Buffer
	ast.Walk(func(node *blackfriday.Node, entering bool) blackfriday.WalkStatus {
		if node.Type == blackfriday.Paragraph {
			if entering {
				buff.WriteString("<p class=\"markdown-paragraph\">")
			} else {
				buff.WriteString("</p>\n")
			}
		} else {
			renderer.RenderNode(&buff, node, entering)
		}
		return blackfriday.GoToNext
	})
	unsafeHTML := buff.Bytes()
	sanitizedBytes := sanitizer.SanitizeBytes(unsafeHTML)

	return strings.TrimSpace(string(sanitizedBytes))
}

func Sanitize(html string) string {
	sanitizedBytes := sanitizer.SanitizeBytes([]byte(html))
	return string(sanitizedBytes)
}

func CutMarkdown(markdown string) string {
	markdown = strings.TrimSpace(markdown)
	unsafeHTML := blackfriday.Run([]byte(markdown), blackfriday.WithExtensions(extensions))
	sanitizedBytes := bluemonday.StrictPolicy().SanitizeBytes(unsafeHTML)
	return strings.TrimSpace(string(sanitizedBytes))
}
func CutHTML(html string) string {
	sanitizedBytes := bluemonday.StrictPolicy().SanitizeBytes([]byte(html))
	return string(sanitizedBytes)
}

// getImagesFromMarkdown returns a slice of image paths
func getImagesFromMarkdown(markdown string) []string {
	r, _ := regexp.Compile(`!\[(.*?)\]\((.*?)\)`)

	parsedImages := r.FindAllStringSubmatch(markdown, -1)

	images := make([]string, 0)
	for _, markdownImage := range parsedImages {
		images = append(images, markdownImage[2])
	}
	return images
}

// GetImagesFromMarkdown returns a slice of images, prepending the dataDir path if necessary
func GetImagesFromMarkdown(markdown string) []string {
	r, _ := regexp.Compile(`!\[(.*?)\]\((.*?)\)`)

	parsedImages := r.FindAllStringSubmatch(markdown, -1)

	images := make([]string, 0)
	for _, markdownImage := range parsedImages {
		// differentiate between local images and remote images
		if strings.HasPrefix(markdownImage[2], "projects/") {
			images = append(images, path.Join(config.Get().DataDir, markdownImage[2]))
		} else {
			images = append(images, markdownImage[2])
		}
	}
	return images
}

// addDatadirToAttachments adds the datadir path to any image attachments
func addDataDirToAttachments(markdown string) string {
	imagePaths := removeDuplicates(getImagesFromMarkdown(markdown))

	for _, image := range imagePaths {
		if strings.HasPrefix(image, "projects/") {
			markdown = strings.Replace(markdown, image, path.Join(config.Get().DataDir, image), -1)
		}
	}

	return markdown
}

func removeDuplicates(s []string) []string {
	seen := make(map[string]struct{}, len(s))
	j := 0
	for _, v := range s {
		if _, ok := seen[v]; ok {
			continue
		}
		seen[v] = struct{}{}
		s[j] = v
		j++
	}
	return s[:j]
}
