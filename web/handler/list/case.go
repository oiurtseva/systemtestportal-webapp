/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CasesGet simply serves a page that is used to list testcases,
func CasesGet(lister handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcs, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTestCaseListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestCases, tcs).
			With(context.DeleteLabels, modal.LabelDeleteMessage), tmpl, w, r)
	}
}

// IsJSON checks whether a given string is in JSON format
func IsJSON(str string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(str), &js) == nil
}

// getTestCaseListFragment returns either only the test case list fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesListFragment(r)
	}
	return getTabTestCasesListTree(r)
}

// getTabTestCasesListTree returns the test case list tab template with all parent templates
func getTabTestCasesListTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test case list tree
		Append(templates.TestCasesList, templates.ManageLabels, templates.DeleteConfirm).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesListFragment returns only the test case list tab template
func getTabTestCasesListFragment(r *http.Request) *template.Template {
	return handler.GetBaseTree(r).
		Append(templates.TestCasesList, templates.ManageLabels, templates.DeleteConfirm).
		Get().Lookup(templates.TabContent)
}
