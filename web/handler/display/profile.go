/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package display

import (
	"html/template"
	"net/http"

	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

func ShowProfileGet(ul handler.UserLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		params := httptreemux.ContextParams(r.Context())
		requestedUserName := params["profile"]

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		var targetUser *user.User = nil
		for _, user := range users {
			if user.Name == requestedUserName {
				targetUser = user
			}
		}

		if targetUser == nil {
			errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
			return
		}

		tmpl := getShowProfileTree(r)
		if currentUser != nil && targetUser.ID() == currentUser.ID() {
			//Show Profile with edit options
			handler.PrintTmpl(context.New().
				WithUserInformation(r).
				With(context.Profile, targetUser).
				With(context.Edit, true), tmpl, w, r)
		} else {
			if !targetUser.IsShownPublic {
				errors.Handle(errors.ConstructStd(http.StatusForbidden, "Profile not public", "The requested user profile is private", r).Finish(), w, r)
				return
			} else {
				handler.PrintTmpl(context.New().
					WithUserInformation(r).
					With(context.Profile, targetUser), tmpl, w, r)
			}
		}
	}
}

func EditProfileGet(ul handler.UserLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		currentUser := handler.GetContextEntities(r).User

		params := httptreemux.ContextParams(r.Context())
		requestedUserName := params["profile"]

		users, err := ul.List()
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		var targetUser *user.User = nil
		for _, user := range users {
			if user.Name == requestedUserName {
				targetUser = user
			}
		}

		if targetUser == nil {
			errors.Handle(errors.ConstructStd(http.StatusNotFound, "Profile not found", "The user you requested does not exist", r).Finish(), w, r)
			return
		}

		if (currentUser != nil && targetUser.ID() != currentUser.ID()) || currentUser == nil {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tmpl := getEditProfileTree(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Profile, targetUser), tmpl, w, r)

	}
}

// getAboutTree returns the about screen template with all parent templates
func getShowProfileTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		Append(templates.Profile).
		Get().Lookup(templates.HeaderDef)
}

func getEditProfileTree(r *http.Request) *template.Template {
	return handler.GetNoSideBarTree(r).
		Append(templates.AccountSettings).
		Get().Lookup(templates.HeaderDef)
}
