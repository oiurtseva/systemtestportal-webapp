/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"net/http"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/store"

	"net/url"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestShowCaseGet(t *testing.T) {
	store.InitializeTestDatabase()
	commentStoreMock := &handler.CommentStoreMock{}
	taskAssigneeMock := &handler.TaskGetterMock{}
	testCaseListerMock := &handler.CaseListerMock{}

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  nil,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProjectPrivate,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUserUnauthorized,
		},
	)

	tested := ShowCaseGet
	handler.Suite(t,
		handler.CreateTest("Empty context",
			handler.ExpectResponse(
				tested(commentStoreMock, taskAssigneeMock, testCaseListerMock),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(
				tested(commentStoreMock, taskAssigneeMock, testCaseListerMock),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			handler.ExpectResponse(
				tested(commentStoreMock, taskAssigneeMock, testCaseListerMock),
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested(commentStoreMock, taskAssigneeMock, testCaseListerMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestCreateCaseGet(t *testing.T) {
	store.InitializeTestDatabase()
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	caseListerMock := &handler.CaseListerMock{}

	tested := CreateCaseGet
	handler.Suite(t,
		handler.CreateTest("Empty context",
			handler.ExpectResponse(
				tested(caseListerMock),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(
				tested(caseListerMock),
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			handler.ExpectResponse(
				tested(caseListerMock),
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested(caseListerMock),
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestEditCaseGet(t *testing.T) {
	store.InitializeTestDatabase()
	inavlidVersionParams := url.Values{}
	inavlidVersionParams.Add(httputil.Version, "-1")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  nil,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	testCaseListerMock := &handler.CaseListerMock{}
	tested := EditCaseGet(testCaseListerMock)
	handler.Suite(t,
		handler.CreateTest("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No user in context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid version number",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusBadRequest),
			),
			handler.SimpleRequest(ctx, http.MethodGet, inavlidVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, inavlidVersionParams),
		),
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestHistoryCaseGet(t *testing.T) {
	store.InitializeTestDatabase()
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  nil,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProjectPrivate,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUserUnauthorized,
		},
	)

	tested := HistoryCaseGet
	handler.Suite(t,
		handler.CreateTest("Empty context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusInternalServerError),
			),
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusForbidden),
			),
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			handler.ExpectResponse(
				tested,
				handler.HasStatus(http.StatusOK),
			),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
