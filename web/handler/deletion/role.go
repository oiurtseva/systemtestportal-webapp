/*
   This file is part of SystemTestPortal.
   Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

   SystemTestPortal is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   SystemTestPortal is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package deletion

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

const (
	errCouldNotDecodeRolesTitle = "Couldn't delete role."
	errCouldNotDecodeRoles      = "We were unable to decode the change to role " +
		"send in your request. This is most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

func ProjectRoleDelete(projectAdder handler.ProjectAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		var input string

		if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeRolesTitle, errCouldNotDecodeRoles, r).
				WithLog("Couldn't read role from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		delete(c.Project.Roles, project.RoleName(input))

		if err := projectAdder.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)

	}

}
