/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
)

func TestSystemSettingsSQL(t *testing.T) {
	SystemSettingsStore := SystemSettingsSQL{emptyEngine(t)}

	systemSettingsInstance := getSystemSettingsInstance()

	// Put it in the db
	err := SystemSettingsStore.InsertSystemSettings(systemSettingsInstance)
	if err != nil {
		t.Errorf("%s", "Failed to insert system settings")
	}

	// Get it from the db
	systemSettingsFromDb, err := SystemSettingsStore.GetSystemSettings()
	if err != nil {
		t.Errorf("%s", "Failed to get system settings")
	}

	// Check if its still the same
	// Reset the timestamp because it will be wrong
	systemSettingsFromDb.GlobalMessageExpirationDate = systemSettingsInstance.GlobalMessageExpirationDate
	if !reflect.DeepEqual(systemSettingsFromDb, systemSettingsInstance) {
		t.Errorf("Expected \n%v\n, got \n%v", systemSettingsInstance, systemSettingsFromDb)
	}

	// Change the local instance
	systemSettingsInstance.GlobalMessage = "TEST"

	// Update the db
	err = SystemSettingsStore.UpdateSystemSettings(systemSettingsInstance)
	if err != nil {
		t.Errorf("%s", "Failed to update the db SystemSettings")
	}

	// Get it again from the db
	systemSettingsFromDb, err = SystemSettingsStore.GetSystemSettings()
	if err != nil {
		t.Errorf("%s", "Failed to get SystemSettings after update")
	}

	if systemSettingsFromDb.GlobalMessage != systemSettingsInstance.GlobalMessage {
		t.Errorf("%s", "Message from the DB wasn't the same")
	}
}

func TestSystemSettingsSQL_AddSystemSettings(t *testing.T) {

}

func getSystemSettingsInstance() *system.SystemSettings {
	systemSettings := system.SystemSettings{
		IsDisplayGlobalMessage:        true,
		IsAccessAllowed:               true,
		IsRegistrationAllowed:         true,
		IsEmailVerification:           true,
		IsDeleteUsers:                 true,
		IsGlobalMessageExpirationDate: true,
		IsExecutionTime:               true,

		GlobalMessage:                   "GlobalMessage",
		GlobalMessageType:               "hello",
		GlobalMessageTimeStamp:          "hello",
		GlobalMessageExpirationDate:     time.Now(),
		GlobalMessageExpirationDateUnix: "123123123",
	}

	return &systemSettings
}
