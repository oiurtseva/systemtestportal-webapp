# Use Gometalinter

## Context and Problem Statement

We need to execute the source linter used by Gitlab locally on our machines before committing.

## Decision Drivers

* Code Quality
* Personal interest in how to become a better go developer

## Considered Options

* Run gometalinter local manually
* Run gometalinter local automatically

## Decision Outcome

No outcome yet. Description will be enhanced.

### Linux manually

1. Find out the local path of this project. We will call it `<CWD>` (current working directory)
2. In the `<CWD>` run: 

    ```text
    go get -u github.com/alecthomas/gometalinter
    gometalinter --config='gometalinter.conf' <CWD>/... | sort -t: -k '1,1' -k '2,2g' -k '3,3g' > <CWD>/linter.log
    ```
3. This will take some time but it will produce a `linter.log` file in the root directory.

### Windows manually

1. Find out the local path of this project. We will call it `<CWD>` (current working directory)
2. In the `<CWD>` run: 

    ```text
    go get -u github.com/alecthomas/gometalinter
    gometalinter --config='gometalinter.conf' <CWD>/...
    ```
3. This will print out the linters output on command line.
