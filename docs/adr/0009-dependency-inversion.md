# Follow Depedency Inversion

## Context and Problem Statement

We want to stay consistent with the use of interfaces while avoiding circle dependencies and prioritizing business logic in higher level packages.


## Considered Options

* Dependency Inversion
* "Regular" dependencies

## Decision Outcome

Chosen option: "Dependency Invesion", because it allows us to define required functionality in higher level components, while leaving lower level components with the implementation. This allows us to easily switch out lower level components, e.g. 'store', without touching higher level components, e.g. 'handler'.

Positive Consequences: <!-- optional -->
* Follow the SOLID principles
* Atomar interfaces defined at the point of use 
* Higher level components don't need to worry about lower level components

Negative consequences: <!-- optional -->
* Somewhat confusing
* Lots of parameters


## Links 

* [SOLID - Dependency Inversion](https://en.wikipedia.org/wiki/Dependency_inversion_principle)
