/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");


/* this method is used to jump to a selected project in the explore projects list */
function useProjectLink(event) {
    const url = urlify("/" + event.currentTarget.title + "/" + event.currentTarget.id + "/");
    window.location.href = url.toString();
}

/* this method is used to jump to a selected group in the explore groups list */
function useGroupLink(event) {
    const url = urlify("/" + event.currentTarget.id + "/");
    window.location.href = url.toString();
}

function useUserLink(event) {
    const url = urlify("/users/" + event.currentTarget.id + "/");
    window.location.href = url.toString();
}
