{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}
{{define "comments" }}

    <!-- Existing Comments -->
    <label><strong>{{T "Comments" .}}</strong></label>

    <div class="form-group">
        <ul class="list-group" id="commentListGroup" data-children=".li">
            {{if .Comments}}
                {{range $index, $element := .Comments}}
                    {{ template "comment" dict "Comment" . "SystemSettings" $.SystemSettings }}
                {{end}}
            {{end}}
        </ul>

        {{if not .Comments}}
            <p id="commentsEmptyLine" class="text-muted">{{T "There are no comments yet" .}}.</p>
        {{end}}
    </div>

    <!-- New Comment -->
    {{if .SignedIn }}
        <div class="form-group">
            <label><strong>{{T "New Comment" .}}</strong></label>
            <div class="form-group">
                <div class="md-area">
                        <textarea class="form-control markdown-textarea" style="resize: none" id="inputCommentField-New"
                                  placeholder="Leave a comment" type="text" rows="3" maxlength="1000"
                                  aria-describedby="commentAreaDescriptor"></textarea>
                    <div class="md-area-bottom-toolbar">
                        <div style="float:left">{{T "Markdown supported" .}}</div>
                    </div>
                </div>
                <div class="mb-2 row">
                    <div class="col-8">
                        <small class="text-muted">
                            <i>{{T "You have" .}} <span id="newCommentCharacterLeft">1000</span> {{T "characters left" .}}.</i>
                        </small>
                    </div>
                    <div class="col-4 mt-2 ">
                        <button id="submitComment" class="btn btn-primary float-right disabled cursor-not-clickable" type="button"
                                data-original-title="{{T "You have to enter text to make a comment" .}}"
                                data-toggle="tooltip" title="">{{T "Comment" .}}</button>
                    </div>
                </div>
            </div>
        </div>
    {{else}}
    <p class="text-muted">
        <a data-toggle="modal" data-target="#signin-modal" href="">{{T "Sign in" .}}</a> {{T "to comment" .}}.
    </p>
    {{end}}

    <!-- Import Scripts here -->
    <script src="/static/js/project/comments.js" integrity="{{sha256 "/static/js/project/comments.js"}}"></script>

    <script>
        initializeNewCommentsClickListener();
        $("time.timeago").timeago();
    </script>
{{end}}