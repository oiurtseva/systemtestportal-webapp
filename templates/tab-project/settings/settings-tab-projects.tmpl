{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

<!-- Translated to German -->
{{define "settings-tab"}}
{{template "modal-delete-confirm" .DeleteProject}}
{{template "modal-generic-error" .}}

    <div id="project-settings-content">

        <!-- Image + Upload -->
        <div class="row tab-side-bar-row">
            <div class="col-md-4">
                <div class="d-flex project-small-thumbnail mb-3 mr-3">
                    <img id="inputProjectImage" alt="project image" class="rounded" src="{{getImagePath .Project.Image "project"}}">
                </div>
            </div>
            <div class="col-md-7 d-flex">
                <div class="d-none d-sm-block tab-side-bar mr-3"></div>
                <label class="btn btn-secondary btn-file mt-auto">{{T "Upload file" .}}
                    <input style="display: none;" class="" type="file" accept=".png,.jpg,.jpeg">
                </label>
            </div>
        </div>

        <div class="row-md-8 tab-side-bar-row">
            <div class="form-group p-3">
                <label for="inputProjectName"><strong>{{T "Project name" .}}</strong></label>
                <div data-toggle="tooltip"
                     title="" data-placement="top"
                     data-original-title="This feature will be available in a future version">
                    <input disabled class="form-control" id="inputProjectName"
                           value="{{ .Project.Name }}"/>
                </div>
            </div>
            <div class="form-group p-3">
                <label for="inputProjectDesc"><strong>{{T "Project description" .}}</strong></label>
                <div class="md-area">
                        <textarea class="form-control markdown-textarea" id="inputProjectDescription" style="resize: none;"
                                  placeholder="" rows="4" maxlength="250">{{ .Project.Description }}</textarea>
                    <div class="md-area-bottom-toolbar">
                        <div style="float:left">Markdown supported</div>
                    </div>
                </div>
            </div>
            <div class="form-group p-3">
                <label><strong>{{T "Visibility" .}}</strong></label>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input no-help" name="optionsProjectVisibility"
                               id="optionPublic"
                               value="public"
                        {{ if eq .Project.Visibility 1 }}
                               checked
                        {{ end }}
                        >
                        <b>{{T "public" .}}</b> &mdash; {{T "public projects are visible to everyone" .}}
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input no-help" name="optionsProjectVisibility"
                               id="optionInternal"
                               value="internal"
                        {{ if eq .Project.Visibility 2 }}
                               checked
                        {{ end }}
                        >
                        <b>{{T "internal" .}}</b> &mdash; {{T "internal projects are visible to all signed in users" .}}
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input no-help" name="optionsProjectVisibility"
                               id="optionPrivate"
                               value="private"
                        {{ if eq .Project.Visibility 3 }}
                               checked
                        {{ end }}
                        >
                        <b>{{T "private" .}}</b> &mdash; {{T "private projects are only visible to members of these projects" .}}
                    </label>
                </div>
            </div>
        </div>
    </div>

<!-- Import Scripts here -->
<script src="/static/js/project/settings/settings-project.js" integrity="{{sha256 "/static/js/project/settings/settings-project.js"}}"></script>
<script src="/static/js/util/generate_help.js" integrity="{{sha256 "/static/js/util/generate_help.js"}}"></script>

<script>
    initializeProjectSettingsListener();
    generateHelp($("#settings-nav"));
</script>
{{end}}